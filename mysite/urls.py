from django.contrib import admin
from django.urls import path, include
from mysite import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('kontakt/', views.onas),
    path('', views.blog),
    path('historia/', views.historia),
    path('stadion/', views.stadion),
    path('kadra/', views.kadra),
    
    path('wpisy/', include('wpisy.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

