from django.http import HttpResponse
from django.shortcuts import render

def blog(request):
    return render(request, 'blog.html')


def onas(request):
    return render(request, 'kontakt.html')
    
def historia(request):
    return render(request, 'historia.html')

def kadra(request):
    return render(request, 'kadra.html')

def stadion(request):
    return render(request, 'stadion.html')
